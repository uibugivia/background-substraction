project(npbgf)

cmake_minimum_required(VERSION 2.8)
aux_source_directory(./src SRC_LIST)
aux_source_directory(./src/support SRC_LIST)
aux_source_directory(./src/bg_substraction SRC_LIST)

set(HEADER_FILES src/Controls.h src/support/Utils.h)

set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

add_executable(${PROJECT_NAME} ${SRC_LIST} ${HEADER_FILES})
find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )


target_link_libraries( ${PROJECT_NAME} ${OpenCV_LIBS} )
