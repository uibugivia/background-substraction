/*subtractor.h (c) by Gabriel Moya

subtractor.h  is licensed under a
Creative Commons Attribution-NonCommercial 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nc/4.0/>.

Gabriel Moya
 *
 * Universitat de les illes Balears
 * UGIVIA
 * E-mail:  gabriel.moya@uib.es
*/


#ifndef SUBTRACTOR_H
#define SUBTRACTOR_H


// OpenCV
#include <opencv2/opencv.hpp>


#include "../Controls.h"
#include "bgmodel.h"
#include "KernelTable.h"

using namespace cv;
using namespace std;

#define 	KERNELHALFWIDTH 255
#define 	SEGMAMAX 36.5
#define	    SEGMAMIN 0.5
#define		SEGMABINS 80
#define		DEFAULTSEGMA 1.0

class Subtractor
{
public:
    Subtractor(PlayControls);
    void buildModel();

    void subtraction(unsigned int *frame, float *pImage);
    void thresholdImage(float *pImage, unsigned char *thImage, unsigned int *frame, bool showSleep);
    void update(unsigned int *frame, float *mask);

    unsigned int *transform;
    BgModel *bg;

private:

    PlayControls pc;

    KernelLUTable * KernelTable;
    KernelLUTable * KernelTableD;

    unsigned char *nanMask;
    float *sleepMask;
    void drawDensityEstimation(unsigned int *frame, int ip, float val);
    void estimation();

};

#endif // SUBTRACTOR_H
