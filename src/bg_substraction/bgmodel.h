/*bgmodel.h (c) by Gabriel Moya>

bgmodel.h is licensed under a
Creative Commons Attribution-NonCommercial 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nc/4.0/>.

Gabriel Moya
 *
 * Universitat de les illes Balears
 * UGIVIA
 * E-mail:  gabriel.moya@uib.es
*/


#ifndef BGMODEL_H
#define BGMODEL_H

#include "../Controls.h"

#define alpha 0.02f

class BgModel
{
public:
    BgModel(PlayControls pc);

    void buildHist();
    void calculateCDF();
    void estimateDepthSD();
    void addFrame(unsigned int *frame);
    void updateMask(unsigned char *mask);

    float    *getCDF();
    float    *getADOmodel();
    unsigned int *getModel();
    unsigned int *getTop();
    unsigned int *getTopD();

    PlayControls pc;
    unsigned int *previous;
    unsigned int *pSDs;
    unsigned char *medianBins;

    float *AdoModel;


    int histBins;
    int sizeChannels;


private:


    int modelLength;

    float *cdf;
    unsigned int *top;
    unsigned int *topD;


    unsigned int *model;
    unsigned int *diffHist;


};

#endif // BGMODEL_H
