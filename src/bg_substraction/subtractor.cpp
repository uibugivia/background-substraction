/*subtractor.cpp (c) by Gabriel Moya
 *
subtractor.cpp  is licensed under a
Creative Commons Attribution-NonCommercial 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nc/4.0/>.

Gabriel Moya
 *
 * Universitat de les illes Balears
 * UGIVIA
 * E-mail:  gabriel.moya@uib.es
*/


#include "subtractor.h"
#include "../support/Utils.h"
#include "../Controls.h"

using namespace useful_functions;


Subtractor::Subtractor(PlayControls pc) {
    bg = new BgModel(pc);
    this->pc = pc;
    transform = invD();
    nanMask = new unsigned char[pc.imageSize];
    sleepMask = new float[pc.imageSize];
}

void Subtractor::buildModel() {
    int endTraining = pc.StartFrame + pc.TrainingFrames;

    unsigned int *input = new unsigned int[pc.imageSize * 3];
    unsigned short *depth = new unsigned short[pc.imageSize];
    cout << "lastFrame: " << endTraining << endl;
    for (int i = pc.StartFrame; i < endTraining; i++) {
        string im_color_name = pc.files_color[i];
        if (im_color_name[0] == '.') {
            continue;
        }
        string im_depth_name = "d" + im_color_name.substr(2, im_color_name.size());
        cout << "Training: " << i << endl;

        //Get Images
        Mat color = imread(pc.path_im_color + im_color_name);
        Mat depthImg = imread(pc.path_im_depth + im_depth_name, CV_LOAD_IMAGE_ANYDEPTH);

        depthMat2array(depthImg, depth);

        //Training
        BGR2SnGnRn(color.data, depth, input, transform, pc.rows, pc.cols);

        //Inpainting first image
        if (i == pc.StartFrame) {
            int s = 0;
            Mat aux(pc.rows, pc.cols, CV_16U);
            for (int x = 0; x < pc.rows; ++x) {
                for (int y = 0; y < pc.cols; ++y) {
                    aux.at<ushort>(x, y) = input[s + 2];
                    s += 3;
                }
            }

            double minVal, maxVal;
            Point minLoc, maxLoc;
            cv::minMaxLoc(aux, &minVal, &maxVal, &minLoc, &maxLoc);

            Mat inpaintMask, dst;
            cv::normalize(aux, dst, 0, 255, NORM_MINMAX, CV_8UC1);
            cv::threshold(dst, inpaintMask, 0, 255, THRESH_BINARY_INV);
            Mat inpainted;
            cv::inpaint(dst, inpaintMask, inpainted, 2, INPAINT_TELEA);

            s = 0;
            for (int x = 0; x < pc.rows; x++) {
                for (int y = 0; y < pc.cols; y++) {
                    input[s + 2] = ((inpainted.at<uchar>(x, y) / 255.0) * (maxVal - minVal)) + minVal;
                    s += 3;
                }
            }
        }

        bg->addFrame(input);
    }

    estimation();
}


void Subtractor::estimation() {
    KernelTable = new KernelLUTable(KERNELHALFWIDTH, SEGMAMIN, SEGMAMAX, SEGMABINS);
    KernelTableD = new KernelLUTable(255, 2, 32, 64.00);

    bg->buildHist();
    bg->calculateCDF();
    bg->estimateDepthSD();
}

void Subtractor::drawDensityEstimation(unsigned int *frame, int ip, float val) {
    PlayControls pc = pc;
    Mat hist = Mat(960, 1280, CV_8UC3, Scalar::all(0));

    cout << ip << endl;
    putText(hist, float2String(val), Point(1100, 50), FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 2, LINE_AA);

    //Subtraction constants
    unsigned int *pSequence = bg->getModel();
    unsigned int SampleSize = pc.TrainingFrames;
    int KernelHalfWidth = KernelTable->tablehalfwidth;
    unsigned int kerneltablewidth = 2 * KernelHalfWidth + 1;


    //loop variables
    int g, k;
    double *kerneltable = KernelTable->kerneltable;

    int i = ip * 3;
    float res[] = {0.0, 0.0, 0.0};
    if (frame[i + 2] == pc.NaN) {
        frame[i + 2] = bg->previous[i + 2];
    }
    unsigned int SDbins = 3;//(unsigned int) floor(((DEFAULTSEGMA-SEGMAMIN)*SEGMABINS)/(SEGMAMAX-SEGMAMIN));
    int kernelbase = SDbins * kerneltablewidth;
    int kernelbase3 = bg->pSDs[ip] * (2 * (KernelTableD->tablehalfwidth) + 1);
    int width = 1280 / (1050 - 650);
    int cWidth = 640 / (255);
    int accWidth = 0;

    for (int x = 650; x <= 1050; x++) {
        int j = 0;
        float sum[] = {0.0, 0.0, 0.0};
        double kernel;

        while (j < SampleSize) {
            //d
            int base = j * pc.imageSize * 3 + i;
            g = pSequence[base + 2];
            k = (g - x) + KernelTableD->tablehalfwidth;
            kernel = kerneltable[kernelbase3 + k];
            sum[2] += kernel;

            j++;
        }
        int aux = accWidth;
        accWidth += width;
        float ss = sum[2];

        rectangle(hist, Point(aux, 480),
                  Point(accWidth, 480 - (480 * ss / j)),
                  Scalar(200, 200, 200),
                  CV_FILLED);

        if (x == frame[i + 2]) {
            circle(hist, Point(aux + (width / 2), 480 - (480 * ss / j)), 4, Scalar(33, 45, 200), CV_FILLED);
            putText(hist, float2String(ss), Point(1100, 100), FONT_HERSHEY_SIMPLEX, 0.8, Scalar(33, 45, 200), 2,
                    LINE_AA);
            res[0] = ss;
        }
    }

    accWidth = 0;

    for (int x = 0; x <= 255; x++) {
        int j = 0;
        float sum[] = {0.0, 0.0, 0.0};
        double kernel;

        while (j < SampleSize) {

            int base = j * pc.imageSize * 3 + i;

            //r
            int g0 = pSequence[base];
            k = (g0 - x) + KernelHalfWidth;
            kernel = kerneltable[kernelbase + k];
            sum[0] += kernel;

            //g
            int g1 = pSequence[base + 1];
            k = (g1 - x) + KernelHalfWidth;
            kernel = kerneltable[kernelbase + k];
            sum[1] += kernel;

            j++;
        }

        int aux = accWidth;
        accWidth += cWidth;
        float s0 = sum[0];

        float s1 = sum[1];

        rectangle(hist, Point(aux + 640, 960),
                  Point(accWidth + 640, 960 - (960 * s0 / j)),
                  Scalar(200, 200, 200),
                  CV_FILLED);
        rectangle(hist, Point(aux, 960),
                  Point(accWidth, 960 - (960 * s1 / j)),
                  Scalar(200, 200, 200),
                  CV_FILLED);

        if (x == frame[i]) {
            circle(hist, Point(aux + 640 + (width / 2), 960 - (960 * (s0 / j))), 4, Scalar(200, 45, 2), CV_FILLED);
            putText(hist, float2String(s0), Point(1100, 150), FONT_HERSHEY_SIMPLEX, 0.8, Scalar(200, 45, 2), 2,
                    LINE_AA);
            res[1] = s0;

        }
        if (x == frame[i + 1]) {
            //cout << s1 << endl;
            circle(hist, Point(aux + (width / 2), 960 - (960 * (s1 / j))), 4, Scalar(45, 200, 2), CV_FILLED);
            putText(hist, float2String(s1), Point(1100, 200), FONT_HERSHEY_SIMPLEX, 0.8, Scalar(45, 200, 2), 2,
                    LINE_AA);
            res[2] = s1;

        }

    }
    float va = (res[2] * res[0] * res[1]) / (float) SampleSize;
    putText(hist, float2String(va), Point(1100, 250), FONT_HERSHEY_SIMPLEX, 0.8, Scalar(45, 200, 2), 2, LINE_AA);

    //imshow("hist", hist);
    //waitKey(0);
}


void Subtractor::subtraction(unsigned int *frame, float *pImage) {
//    PlayControls pc = pc;
    cout << "substracting bg 0" << endl;
    fill(nanMask, nanMask + pc.imageSize, 1);
    fill(sleepMask, sleepMask + pc.imageSize, 0);
    //Subtraction constants
    float sum = 0;
    float sleepProb = 0.0;
    unsigned int i, j;

    unsigned int *pSequence = bg->getModel();
    unsigned int SampleSize = pc.TrainingFrames;

    float *ADOmodel = bg->getADOmodel();
    float *CDF = bg->getCDF();

    //LUT constants
    double *kerneltable = KernelTable->kerneltable;
    int KernelHalfWidth = KernelTable->tablehalfwidth;

    unsigned int kerneltablewidth = 2 * KernelHalfWidth + 1;
    //loop variables
    int g, k, ip;
    int kernelbase3;
    double kernel1, kernel2, kernel3;
    double p;
    //used estimated kernel width to access the right kernel
    unsigned int SDbins = 3; //(unsigned int) floor(((DEFAULTSEGMA-SEGMAMIN)*SEGMABINS)/(SEGMAMAX-SEGMAMIN));
    //cout << "-->" << SDbins << endl;
    int kernelbase1 = SDbins * kerneltablewidth;
    int kernelbase2 = SDbins * kerneltablewidth;
    cout << "substracting bg 1" << endl;
    //int coord = 19478;//(313*640)+263;//(263, 313)
    for (i = 0, ip = 0; i < pc.imageSize * 3; i += 3, ip++) {
        j = SampleSize - 1;
        sum = 0.0f;
        sleepProb = 0;
        sleepMask[ip] = 0;
        float partialSleep = 0;

//        if(coord == ip) cout<< "FRAME: " << frame[i] << ": " << frame[i+1] <<": " << frame[i+2] << endl;

        kernelbase3 = bg->pSDs[ip] * (2 * (KernelTableD->tablehalfwidth) + 1);
        //ADO pixels
        if (frame[i + 2] == pc.NaN && ADOmodel[ip] < pc.AdoThreshold) {

            nanMask[ip] = 0;
        } else {
            //Nan non ADO pixels
            if (frame[i + 2] == pc.NaN) {
                frame[i + 2] = bg->previous[i + 2];
                //if(coord == ip) cout<< "Nan non ado: "<< frame[i+2] << endl;

            }
            float sum[] = {0.0, 0.0, 0.0};
            float val = 0;

            int count = 0;

            while ((j > 0) &&
                   (val <= pc.ThresholdValue1) /*&&
                   (partialSleep <= pc.SleepinThreshold)*/) {

                int base = j * pc.imageSize * 3 + i;
                //r
                int g0 = pSequence[base];
                k = (g0 - frame[i]) + KernelHalfWidth;
                sum[0] += kerneltable[kernelbase1 + k];
                //g
                int g1 = pSequence[base + 1];
                k = (g1 - frame[i + 1]) + KernelHalfWidth;
                sum[1] += kerneltable[kernelbase2 + k];
                //d
                g = pSequence[base + 2];
                k = (g - frame[i + 2]) + KernelTableD->tablehalfwidth;
                sum[2] += kerneltable[kernelbase3 + k];


                //Sleeping objects
                int diff = (g - frame[i + 2]);

                if (diff < 0) {
                    if (diff > bg->histBins) diff = bg->histBins - 1;
                    sleepProb += CDF[(ip * bg->histBins) + diff];
                }
                j--;
                count++;
                val = (sum[0] * sum[1] * sum[2]) / (float) count;

                partialSleep = sleepProb / (float) count;
            }

            p = val; //TOCAT!
            sleepMask[ip] = sleepProb / count;

            pImage[ip] = p;
        }
    }
}

void Subtractor::thresholdImage(float *pImage, unsigned char *thImage, unsigned int *frame, bool showSleep) {

    memset(thImage, 0, pc.imageSize);

    int sleepValue = showSleep ? 191 : 0;

    for (int i = 0; i < pc.imageSize; i++) {
        if (nanMask[i] == 0) thImage[i] = 128;
        else if (pImage[i] < pc.ThresholdValue1) {

            thImage[i] = 255;
            //if(sleepMask[i] < pc.SleepinThreshold)
            //    drawDensityEstimation(frame, i, pImage[i]);
        }
        if (sleepMask[i] > pc.SleepinThreshold) thImage[i] = sleepValue;
    }
}

void Subtractor::update(unsigned int *frame, float *mask) {
    unsigned int *pSequence = bg->getModel();
    unsigned int *top = bg->getTop();
    unsigned int *topD = bg->getTopD();
    unsigned int SampleSize = pc.TrainingFrames;
    unsigned int imageSize = pc.imageSize;

    int i, ip;
    for (i = 0, ip = 0; i < imageSize * 3; i += 3, ip++) {
        int base = top[ip] * imageSize * 3 + i;
        int baseD = topD[ip] * imageSize * 3 + i;
        int s = 1;

        //Color
        if (mask[ip] > pc.ThresholdValue1 || sleepMask[ip] > pc.SleepinThreshold) {

            pSequence[base] = frame[i];
            pSequence[base + 1] = frame[i + 1];

            int newTop = (top[ip] + 1) % SampleSize;
            top[ip] = newTop;
        }
//        //Depth
        if (sleepMask[ip] > pc.SleepinThreshold) {
            if (frame[i + 2] != pc.NaN) {

                bg->previous[i + 2] = frame[i + 2];
            }
            pSequence[baseD + 2] = frame[i + 2];
            int newTop = (topD[ip] + 1) % SampleSize;
            topD[ip] = newTop;

        }
    }
}
