/*bgmodel.cpp (c) by Gabriel Moya

bgmodel.cpp is licensed under a
Creative Commons Attribution-NonCommercial 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nc/4.0/>.

Gabriel Moya
 *
 * Universitat de les illes Balears
 * UGIVIA
 * E-mail:  gabriel.moya@uib.es
*/

#include "bgmodel.h"
#include <string.h>
#include "cmath"

using namespace std;

BgModel::BgModel(PlayControls pc)
{
    this->pc = pc;

    histBins     = 80;
    sizeChannels = pc.imageSize * 3;
    modelLength  = sizeChannels * pc.TrainingFrames;

    topD       = new unsigned int[pc.imageSize];
    top        = new unsigned int[pc.imageSize];
    cdf        = new float[pc.imageSize*histBins]; //only for D
    previous   = new unsigned int[sizeChannels]; //r, g, D
    model      = new unsigned int[modelLength]; //r, g, D * TrainingFrames
    AdoModel   = new float[pc.imageSize]; //D
    pSDs       = new unsigned int[pc.imageSize]; //D
    diffHist   = new unsigned int[sizeChannels * histBins];
    medianBins = new unsigned char[pc.imageSize]; //D
    //TODO: usar fill
    memset(top,        0, sizeof(int) * pc.imageSize);
    memset(topD,       0, sizeof(int) * pc.imageSize);
    memset(cdf,        0, sizeof(float) * pc.imageSize * histBins);
    memset(model,      0, sizeof(int) * modelLength);
    memset(previous,   0, sizeof(int) * sizeChannels );
    memset(diffHist,   0, sizeof(int) * sizeChannels * histBins); //only for D
    memset(AdoModel,   0, sizeof(float) * pc.imageSize);//only for D
    memset(medianBins, 0, pc.imageSize); //only for D
}


void BgModel::addFrame(unsigned int *frame)
{
    //Si es el primer: omplir d-dimension -> Fet a fora.

    //Sino copiar el previous value de la d-dimension i el ADO-model
    int ip,i;

    for(i = 0, ip = 0; i < sizeChannels; i +=3, ip++){
        int s = 0;

        if (frame[i+2] == pc.NaN){
            s = 1;
            frame[i+2] = previous[i+2];
        }
        AdoModel[ip] = (1 - alpha) * AdoModel[ip] + alpha * s;
        int modelBase = top[ip]*sizeChannels;

        model[modelBase + i]   = frame[i];
        model[modelBase + i+1] = frame[i+1];
        model[modelBase + i+2] = frame[i+2];
    }
    memcpy(previous, frame, sizeof(unsigned int)*sizeChannels);

    int newTop = (top[0]+1) % pc.TrainingFrames;
    fill(top, top + pc.imageSize, newTop);
}


void BgModel::buildHist()
{
    unsigned int * image1, * image2;

    for (int i=1; i < pc.TrainingFrames; i++)
    {
        //diff between frame i,i-1;
        image1 = model +(i-1) * sizeChannels;
        image2 = model + i * sizeChannels;

        int histBins_1 = histBins-1;

        for (int j=0; j < sizeChannels; j++)
        {
            int diff = (int)image1[j] - (int) image2[j];

            diff = abs(diff);
            // update histogram
            unsigned int bin=(diff < histBins ? diff : histBins_1);
            diffHist[(j*histBins)+bin]++;
        }
    }
}

//Calculate: CDF and medianBins
void BgModel::calculateCDF(){

    int maxMedian = (pc.TrainingFrames - 1) / 2;
    int i, ip;
    for (i = 0, ip = 0; i < sizeChannels; i+=3, ip++) {

        int sum = 0;
        int histindex = (i+2) * histBins;
        int binindex = ip * histBins;
        bool found = false;

        for (int bin = 0; bin < histBins; bin++){

            sum += diffHist[histindex+bin];
            cdf[binindex + bin] = sum / (float)pc.TrainingFrames;
            if(sum > maxMedian && not found)
            {
                medianBins[ip] = bin;// - 1;
                found = true;
            }
        }
    }
}


void BgModel::estimateDepthSD()
{   //Params en subtractor potser s'ha de passar per param
    float MaxSD = 32;
    float MinSD =  2; //0.5;
    float kernelBins = 64;

    double kernelbinfactor=(kernelBins-1) / (MaxSD-MinSD);

    int j, s;

    for (j = 2,s = 0; j < sizeChannels; j=j+3, s++) {

        int bin = medianBins[s];

        float v = 1.04 * bin;
        v=( v <= MinSD ? MinSD : v);
        // convert sd to kernel table bin
        bin=(int) (v>=MaxSD ? kernelBins-1 : floor((v-MinSD)*kernelbinfactor+.5));
        //assert(bin>=0 && bin < kernelBins );
        pSDs[s]= bin;
    }
}


void BgModel::updateMask(unsigned char *mask)
 {

    //TODO implementar
 }

unsigned int *BgModel::getModel(){

    return model;
}

unsigned int *BgModel::getTop(){

    return top;
}

unsigned int *BgModel::getTopD(){

    return topD;
}

float *BgModel::getADOmodel(){

    return AdoModel;
}

float *BgModel::getCDF(){

    return cdf;

}

