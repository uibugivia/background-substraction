/*main.cpp (c) by Gabriel Moya

KernelTable.cpp  is licensed under a
Creative Commons Attribution-NonCommercial 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nc/4.0/>.

Gabriel Moya
 *
 * Universitat de les illes Balears
 * UGIVIA
 * E-mail:  gabriel.moya@uib.es
*/

#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>

//// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "Controls.h"
#include "support/Utils.h"
#include "bg_substraction/subtractor.h"

#define Q 113

#define D_FORMAT_SINGLE_CHANNEL 0
#define D_FORMAT_2_CHANNEL 2
using namespace std;
//using namespace cv;
using namespace useful_functions;

/*function... might want it in some class?*/
int getdir (string dir, vector<string> &files)
{
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        files.push_back(string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}

Mat readImgDepth(PlayControls pc, const string im_depth_name, unsigned short *depth)
{
    Mat depthImg;
    if (pc.depth_format == D_FORMAT_SINGLE_CHANNEL)
        depthImg = imread(pc.path_im_depth + im_depth_name, CV_LOAD_IMAGE_ANYDEPTH);
    else if (pc.depth_format == D_FORMAT_2_CHANNEL)
        depthImg = imread(pc.path_im_depth + im_depth_name);
    else
        throw 5;

    depthMat2array(depthImg, depth);
    return depthImg;
}

int main(int argc, char** argv)
{
    string path_files = "./data/datasets/OutOfRange/MultiPeople2/";

    PlayControls pc;

    pc.path_files = path_files; //argv[1];
    pc.NaN              = 650; //ADO value for depth images
    pc.rows             = 480;
    pc.cols             = 640;
    pc.imageSize        = pc.rows * pc.cols;
    pc.AdoThreshold     = 0.0050f; //Probability of being ADO pixel
    pc.ThresholdValue1  = 0.00000000001f; //Probability of being background
    pc.SleepinThreshold = 0.60f;
    pc.depth_format = D_FORMAT_SINGLE_CHANNEL;
    pc.path_im_color = path_files + string("input/");
    pc.path_im_depth = path_files + string("depth/");

    getdir(pc.path_im_color, pc.files_color);
    sort(pc.files_color.begin(), pc.files_color.end());

    pc.TrainingFrames = 100;
    pc.StartFrame = 5;

    Subtractor sb(pc);
    sb.buildModel();

    float          *pImage  = new float[pc.imageSize];
    unsigned short *depth   = new unsigned short[pc.imageSize];
    unsigned char  *thImage = new unsigned char[pc.imageSize];
    unsigned int   *input   = new unsigned int[pc.imageSize * 3];

    for (unsigned int i = 80;i < pc.files_color.size();i++) {
        double t = (double)getTickCount();
        string im_color_name = pc.files_color[i];
        if (im_color_name[0]=='.'){
            continue;
        }
        string im_depth_name = "d" + im_color_name.substr(2, im_color_name.size());
        string im_out_name = "p" + im_color_name.substr(2, im_color_name.size());

//        cout << pc.path_im_color + im_color_name << endl;
//        cout << im_depth_name << endl;

        Mat color    = imread(pc.path_im_color + im_color_name);
        Mat depthImg = readImgDepth(pc, im_depth_name, depth);

//        double minVal, maxVal;
//        Point  minLoc, maxLoc;
//        cv::minMaxLoc( depthImg, &minVal, &maxVal, &minLoc, &maxLoc);
//        cout << minVal << endl;
//        cout << maxVal << endl;
//        cout << minLoc << endl;
//        cout << maxLoc << endl;

        // load input
        BGR2SnGnRn(color.data, depth, input, sb.transform, pc.rows, pc.cols);
//        cout << color.rows << endl;
//        cout << color.cols << endl;

        //Background subtraction
        cout << "performing substraction ..." << endl;
        sb.subtraction(input, pImage);

        cout << "thresholding ..." << endl;
        //RESULT VISUALIZATION
        sb.thresholdImage(pImage, thImage, input, false);
        Mat mask_bg = Mat(pc.rows, pc.cols, CV_8UC1, thImage);

        cout << "updating ..." << endl;
        //MODEL UPDATE
        sb.update(input, pImage);

        Mat out(pc.rows, pc.cols, CV_16U, 9000);
        depthImg.copyTo(out, mask_bg);

        Mat im_debug(depthImg.rows, depthImg.cols, CV_8UC3, Scalar(0, 0, 0));
        color.copyTo(im_debug, mask_bg);

        cv::imshow("Result", mask_bg);
        cv::imshow("depth", depthImg);
        cv::imshow("Color", color);
        cv::imshow("out", out);
        cv::imshow("debug", im_debug);

        t = ((double)getTickCount() - t)/getTickFrequency();
        cout << t << endl;

        int key = waitKey(10);
        if(key == Q) break;
        vector<int> compression_params;
        compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
        compression_params.push_back(0);
        cv::imwrite(path_files.append("out/") + im_out_name, out, compression_params);


    }

    return 0;
}

