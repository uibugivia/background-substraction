/*Controls.h (c) by Gabriel Moya

Controls.h is licensed under a
Creative Commons Attribution-NonCommercial 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nc/4.0/>.

Gabriel Moya
 *
 * Universitat de les illes Balears
 * UGIVIA
 * E-mail:  gabriel.moya@uib.es
*/


#ifndef CONTROLS_H
#define CONTROLS_H

#include <iostream>
#include <vector>

using namespace std;

typedef struct {
    //TODO: Inicialitzar thresholds i rows i cols

    string path_files;
    string Path;

    unsigned int StartFrame;
    unsigned int TrainingFrames;

    float ThresholdValue1;
    float SleepinThreshold;
    float AdoThreshold;
    double ThresholdValue3;
    double AlphaValue;

    unsigned int rows;
    unsigned int cols;
    unsigned int NaN;
    unsigned int imageSize;
    vector<string> files_color;
    vector<string> files_depth;

    string path_im_color;
    string path_im_depth;
    unsigned int depth_format;

} PlayControls;
#endif // CONTROLS_H
