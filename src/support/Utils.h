/*Utils.h (c) by Gabriel Moya

Utils.h  is licensed under a
Creative Commons Attribution-NonCommercial 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nc/4.0/>.

Gabriel Moya
 *
 * Universitat de les illes Balears
 * UGIVIA
 * E-mail:  gabriel.moya@uib.es
*/

#include "../Controls.h"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include <string>

using namespace  cv;
using namespace std;


namespace useful_functions
{


extern void ReadParameters(PlayControls *pc);


extern void BGR2SnGnRn(unsigned char * bgrImage,
                unsigned short *depthImage,
                unsigned int  * out_image,
                unsigned int  * dTransform,
                unsigned int rows,
                unsigned int cols);

extern unsigned int * invD();

extern std::string int2String(int x);

extern std::string float2String(float x);

extern void depthMat2array(Mat depthImg, unsigned short *depth);

extern Mat NormalisedProbImage(float *pImage);

extern void save(string path, string folder, int fNumber, Mat img, bool ub);



}
