/*Utils.cpp (c) by Gabriel Moya>

subtractor.cpp  is licensed under a
Creative Commons Attribution-NonCommercial 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nc/4.0/>.

Gabriel Moya
 *
 * Universitat de les illes Balears
 * UGIVIA
 * E-mail:  gabriel.moya@uib.es
*/


#include "Utils.h"


namespace useful_functions {

//
//void ReadParameters(PlayControls *pc)
//{
//    pc->NaN              = 650; //ADO value for depth images
//    pc->rows             = 480;
//    pc->cols             = 640;
//    pc->imageSize        = pc->rows * pc->cols;
//    pc->AdoThreshold     = 0.0050f; //Probability of being ADO pixel
//    pc->TrainingFrames   = 100;
//    pc->ThresholdValue1  = 0.00000000001f; //Probability of being background
//    pc->SleepinThreshold = 0.60f;
//
//    pc->colorExtension   = "Jpeg";
//    pc->depthExtension   = "png";
//    pc->depthFolder      = "depthData";
//    pc->colorFolder      = "colorData";
//    string line = pc->Path + pc->FileName + "/data";
//    pc->Path             = "../../bbdd/";
//
//    ifstream myfile (line.c_str());
//
//    int values[5];
//    if (myfile.is_open()){
//        line  = "";
//        int s = 0;
//        while ( myfile.good()){
//            getline (myfile,line);
//            std::istringstream ss(line);
//            ss >>  values[s++];
//        }
//        myfile.close();
//        pc->StartFrame = values[0];
//        pc->StopFrame  = values[1];
//    }
//}


    void BGR2SnGnRn(unsigned char *bgrImage,
                    unsigned short *depthImage,
                    unsigned int *out_image,
                    unsigned int *dTransform,
                    unsigned int rows,
                    unsigned int cols) {
        double s;
        int d, r2, r3;
        unsigned int i, j;
        unsigned int r, g, b;

        for (i = 0, j = 0; i < (rows * cols * 3); i += 3, j++) {
            b = bgrImage[i];
            g = bgrImage[i + 1];
            r = bgrImage[i + 2];
            d = depthImage[j];
            // calculate color ratios
            s = (double) 255 / (double) (b + g + r + 30);

            r2 = (unsigned int) ((g + 10) * s);
            r3 = (unsigned int) ((r + 10) * s);

            out_image[i] = (unsigned int) (r2 > 255 ? 255 : r2);
            out_image[i + 1] = (unsigned int) (r3 > 255 ? 255 : r3);
            out_image[i + 2] = dTransform[d];
        }
    }


    unsigned int *invD() {
        unsigned int *transform = new unsigned int[10000];

        for (int d = 0; d < 10000; d++) {
            int calc = round(double(1000 - double(3.33 * double(d))) / double(-0.00307 * double(d)));

            if ((calc < 650) || (calc >= 1050)) {
                calc = 650;
            }
            transform[d] = calc;
        }
        return transform;
    }

    Mat NormalisedProbImage(float *pImage) {
        Mat aux = Mat(480, 640, CV_32F, pImage);

        double minVal, maxVal;
        Point minLoc, maxLoc;

        minMaxLoc(aux, &minVal, &maxVal, &minLoc, &maxLoc);

        return aux / maxVal;
    }


    void save(string path, string folder, int fNumber, Mat img, bool ub) {
        Mat dst;
        int thresh = ub ? 129 : 127;
        threshold(img, dst, thresh, 255, THRESH_BINARY);
        string rPath = path + "/" + folder + "/" + int2String(fNumber) + ".png";
        cv::imwrite(rPath, dst);
    }

    /**
     *
     * @param depthImg
     * @param depth
     */
    void depthMat2array(Mat depthImg, unsigned short *depth) {
        std::vector<unsigned short> array;
        array.assign((unsigned short *) depthImg.datastart, (unsigned short *) depthImg.dataend);
        std::copy(array.begin(), array.end(), depth);
    }


    std::string int2String(int x) {
        ostringstream convert;
        convert << x;
        return convert.str();
    }

    std::string float2String(float x) {
        ostringstream convert;
        convert << x;
        return convert.str();
    }


}
