GSM project files
-----------------------------------------------------------------
Note that this code is developed for scientific purposes only and 
it is designed to test the viability of the GSM algorithm.

OpenCV library is needed: http://opencv.org/
-----------------------------------------------------------------

# Background substraction

Background subtraction algorithm in depth images

-> Controls.h

    Configuration file, contains constants all over program.

-> main.cpp

    Receives the sequence to evaluate by command-line argument: Note that the code is 
    constructed taking account that RGB images has the "img_" prefix and the depth 
    images have the "depth_" prefix, as GSM dataset and Camplani dataset.
    
    Contains the main loop. It has four different steps:
        
        - Data acquisition
        - Background subtraction
        - Result visualization
        - Model update

-> subtractor class

    most important class of the project

    important functions:

         - buildModel: contains de training loop
         - subtraction: receives color and depth images and perform de background subtraction
         - update: receives color and probabilistic images and updates the background model
         
-> bgmodel class
    
    Contains all the background model data structures.


-> Kernel table class

    Look up table needed in order to speed up the algorithm performance


-> Utils class

    Contains static useful functions.
